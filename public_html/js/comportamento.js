/**
 * @author Matheus
 */
var alunosJSON = { nome : [], 
matricula : []};

//Aula(AU)
var AUdata;// = new Array();
var AUqtdAulas;// = new Array();
var AUcadastro;// = new Array();
var AUcodTurma;// = new Array();
var AUassunto;// = new Array();

//Frequência(F)
var Fmatricula = new Array();
var Faula_data = new Array();
var Fpresenca = new Array();
var FcodTurma = new Array();

//Boletim(B)
var Bmatricula = new Array();
var BcodDisciplina = new Array();
var Bmedia = new Array();
var Brec = new Array();
var Bfaltas = new Array();
var BcodModalidade = new Array();
var Bn1 = new Array();
var Bn2 = new Array();
var Bn3 = new Array();
var Bn4 = new Array();

//Professor (P)
var Pcad = new Array;
var Psenha = new Array();
var Pnome = new Array();

//Aluno (A)
var Amatricula = new Array();
var Asenha = new Array();
var Anome = new Array();
var Acodturma = new Array;
var Acpf = new Array;

//Curso (C)
var Ccod = new Array();
var Ccurso = new Array();
var Cgrade = new Array();

//Modalidade (M)
var Mcod = new Array();
var Mmodalidade = new Array();
var Mperiodo = new Array();

//grade (G)
var Gcod = new Array();

//disciplina (D)
var Dcod = new Array();
var Ddisc = new Array();
var Dch = new Array();

//turma (T)
var Tcodturma = new Array();
var Tcodcurso = new Array();
var Tturno = new Array();
var Tserie = new Array();

//disciplina-professor (DP)
var DPcodDisc = new Array();
var DPcadProf = new Array();

//disciplina-grade (DG)
var DGcodDisc = new Array();
var DGcodGrade = new Array();

//professor-turma (PT)
var PTcodTurma = new Array();
var PTcadProf = new Array();

//Login
var logSenha = null;
var ok1 = false;
var ok2 = false;
var logid = null;
//Demais Variáveis
var i;
var db = null;
var len = 0;
var presente = new Array;
var assunto = new Array;
var data = new Array;
var qtdAulas = new Array;
var codturma = new Array;
var turmaEscolhida;
var dataSelecionada;

var atualUser;
var atualUserCod;

$(document).ready(function() {
    //cria e se conecta ao bd assim que o DOM estiver pronto
    createDB();
    connectDB();
    //Função submit do login
    $("#login_form").submit(function() {
        while (!ok1 && !ok2) {
            //recupera dados inseridos no login e verifica com os dados obtidos no DB
            logid = $('#login_matricula').val();
            logSenha = $('#login_senha').val();
            len = Pcad.length;
            for (i = 0; i < len; i++) {
                if (Pcad[i] === logid) {
                    logid = logid;
                    ok1 = true;
                    if (Psenha[i] === logSenha) {
                        atualUser=Pnome[i];
                        atualUserCod=Pcad[i];
                        ok2 = true;
                        //Se houver algum registro no BD corresponde aos valores informados no login, redireciona a pagina
                        window.location = "home.html";
                    } else {
                        $("<p> Login e/ou senha incorreto </p>").appendTo('#login_form');
                        break;
                    }
                } else {
                    $("<p> Login e/ou senha incorreto </p>").appendTo('#login_form');
                    break;
                }
            }
        }
    });
    //Submit da chamada
    $("#chamada_form").submit(function() {
        db = openDatabase("bdTeste9app", "1.0", "App Docente", 1024);
        //lista de alunos da chamada
        var aChk = document.getElementsByName("aluno_ausente");
        var data = $("#chamada_data").val();
        var qtdAulas = $("#chamada_qtd_aulas").val();
        var assunto = $("#chamada_assunto").val();
        logid = $('#home_matricula').text();
        //logid não está sendo enxergado aqui pois foi resetado ao redirecionar página
        db.transaction(function(transaction) {
            transaction.executeSql('INSERT INTO aula (data,qtdAulas, codTurma, assunto) VALUES (?, ?, ?, ?)', [data, qtdAulas, turmaEscolhida, assunto]);

            for (var i = 0; i < aChk.length; i++) {
                var matriculabd = aChk[i].id;
                if (aChk[i].checked === true) {
                    //alert("achk cheked");
                    transaction.executeSql('INSERT INTO frequencia (matricula, codTurma ,aula_data, pres) VALUES (?, ?, ? ,"x")', [matriculabd, turmaEscolhida, data]);
                    // CheckBox Marcado... Faça alguma coisa... Ex:
                } else {
                    // CheckBox Não Marcado... Faça alguma outra coisa...
                    //alert("achk not checked");
                    transaction.executeSql('INSERT INTO frequencia (matricula, codTurma ,aula_data, pres) VALUES (?, ?, ? ,"-")', [matriculabd, turmaEscolhida, data]);
                }
            }

        });
        alert("Dados inseridos com sucesso!");
        window.location.reload();

    });
});

function createDB() {
    db = openDatabase("bdTeste9app", "1.0", "App Docente", 1024);

    db.transaction(function(transaction) {

        transaction.executeSql('CREATE TABLE aula(data,qtdAulas, codTurma, assunto)');
        transaction.executeSql('CREATE TABLE frequencia (matricula, codTurma ,aula_data, pres)');

        transaction.executeSql('CREATE TABLE boletim(matricula, codDisciplina, media, rec, faltas, codModalidade, n1, n2, n3, n4)');
    });

    db.transaction(function(transaction) {
        //PROFESSOR
        transaction.executeSql('CREATE TABLE professor(cadastro, nome, senha)');
        transaction.executeSql('INSERT INTO professor(cadastro, nome, senha) VALUES ("p0101", "Valério", "0101")');
        transaction.executeSql('INSERT INTO professor(cadastro, nome, senha) VALUES ("p0102", "Alba", "0102")');
        transaction.executeSql('INSERT INTO professor(cadastro, nome, senha) VALUES ("p0103", "Rafael", "0103")');
        transaction.executeSql('INSERT INTO professor(cadastro, nome, senha) VALUES ("p0104", "Humberto", "0104")');
    });

    db.transaction(function(transaction) {
        //CURSO
        transaction.executeSql('CREATE TABLE curso(codCurso, curso, codGrade)');
        transaction.executeSql('INSERT INTO curso(codCurso, curso, codGrade) VALUES ("c1111", "informática", "g1111")');
        transaction.executeSql('INSERT INTO curso(codCurso, curso, codGrade) VALUES ("c2222", "informática", "g2222")');
        transaction.executeSql('INSERT INTO curso(codCurso, curso, codGrade) VALUES ("c3333", "Administração", "g3333")');
        transaction.executeSql('INSERT INTO curso(codCurso, curso, codGrade) VALUES ("c4444", "Cooperativismo", "g4444")');
    });

    db.transaction(function(transaction) {
        //MODALIDADE
        transaction.executeSql('CREATE TABLE modalidade(codModalidade, modalidade, periodo)');
        transaction.executeSql('INSERT INTO  modalidade(codModalidade, modalidade, periodo) VALUES ("m1111", "Integrado", "anual")');
        transaction.executeSql('INSERT INTO  modalidade(codModalidade, modalidade, periodo) VALUES ("m2222", "Subsequente", "anual")');
        transaction.executeSql('INSERT INTO  modalidade(codModalidade, modalidade, periodo) VALUES ("m3333", "Superior", "anual")');
    });

    db.transaction(function(transaction) {
        //GRADE
        transaction.executeSql('CREATE TABLE grade(codGrade)');
        transaction.executeSql('INSERT INTO  grade(codGrade) VALUES ("g1111")');
        transaction.executeSql('INSERT INTO  grade(codGrade) VALUES ("g2222")');
        transaction.executeSql('INSERT INTO  grade(codGrade) VALUES ("g3333")');
        transaction.executeSql('INSERT INTO  grade(codGrade) VALUES ("g4444")');
    });

    db.transaction(function(transaction) {
        //PROFESSOR-TURMA
        transaction.executeSql('CREATE TABLE professorTurma(cadastro, codTurma)');
        transaction.executeSql('INSERT INTO  professorTurma(cadastro, codTurma) VALUES ("p0101", "t1111")');
        transaction.executeSql('INSERT INTO  professorTurma(cadastro, codTurma) VALUES ("p0101", "t2222")');
        transaction.executeSql('INSERT INTO  professorTurma(cadastro, codTurma) VALUES ("p0102", "t3333")');
        transaction.executeSql('INSERT INTO  professorTurma(cadastro, codTurma) VALUES ("p0102", "t4444")');
        transaction.executeSql('INSERT INTO  professorTurma(cadastro, codTurma) VALUES ("p0103", "t1111")');
        transaction.executeSql('INSERT INTO  professorTurma(cadastro, codTurma) VALUES ("p0103", "t2222")');
        transaction.executeSql('INSERT INTO  professorTurma(cadastro, codTurma) VALUES ("p0104", "t3333")');
        transaction.executeSql('INSERT INTO  professorTurma(cadastro, codTurma) VALUES ("p0104", "t4444")');
    });

    db.transaction(function(transaction) {
        //DISCIPLINA-GRADE
        transaction.executeSql('CREATE TABLE disciplinaGrade(codGrade, codDisciplina)');
        transaction.executeSql('INSERT INTO  disciplinaGrade(codGrade, codDisciplina) VALUES ("g1111", "d1111")');
        transaction.executeSql('INSERT INTO  disciplinaGrade(codGrade, codDisciplina) VALUES ("g1111", "d2222")');
        transaction.executeSql('INSERT INTO  disciplinaGrade(codGrade, codDisciplina) VALUES ("g1111", "d3333")');
        transaction.executeSql('INSERT INTO  disciplinaGrade(codGrade, codDisciplina) VALUES ("g2222", "d1111")');
        transaction.executeSql('INSERT INTO  disciplinaGrade(codGrade, codDisciplina) VALUES ("g2222", "d4444")');
        transaction.executeSql('INSERT INTO  disciplinaGrade(codGrade, codDisciplina) VALUES ("g2222", "d5555")');
        transaction.executeSql('INSERT INTO  disciplinaGrade(codGrade, codDisciplina) VALUES ("g3333", "d2222")');
        transaction.executeSql('INSERT INTO  disciplinaGrade(codGrade, codDisciplina) VALUES ("g3333", "d5555")');
        transaction.executeSql('INSERT INTO  disciplinaGrade(codGrade, codDisciplina) VALUES ("g4444", "d3333")');
        transaction.executeSql('INSERT INTO  disciplinaGrade(codGrade, codDisciplina) VALUES ("g4444", "d4444")');
    });

    db.transaction(function(transaction) {
        //DISCIPLINA
        transaction.executeSql('CREATE TABLE disciplina(codDisciplina, disciplina, ch)');
        transaction.executeSql('INSERT INTO  disciplina(codDisciplina, disciplina, ch) VALUES ("d1111", "Banco de dados", 120)');
        transaction.executeSql('INSERT INTO  disciplina(codDisciplina, disciplina, ch) VALUES ("d2222", "POO", 120)');
        transaction.executeSql('INSERT INTO  disciplina(codDisciplina, disciplina, ch) VALUES ("d3333", "Física", 90)');
        transaction.executeSql('INSERT INTO  disciplina(codDisciplina, disciplina, ch) VALUES ("d4444", "Eletricidade", 150)');
        transaction.executeSql('INSERT INTO  disciplina(codDisciplina, disciplina, ch) VALUES ("d5555", "Ambientes de Prog.", 120)');
    });

    db.transaction(function(transaction) {
        //DISCIPLINA-PROFESSOR
        transaction.executeSql('CREATE TABLE disciplinaProfessor(codDisciplina, cadastro)');
        transaction.executeSql('INSERT INTO  disciplinaProfessor(codDisciplina, cadastro) VALUES ("d1111", "p0101")');
        transaction.executeSql('INSERT INTO  disciplinaProfessor(codDisciplina, cadastro) VALUES ("d2222", "p0101")');
        transaction.executeSql('INSERT INTO  disciplinaProfessor(codDisciplina, cadastro) VALUES ("d1111", "p0102")');
        transaction.executeSql('INSERT INTO  disciplinaProfessor(codDisciplina, cadastro) VALUES ("d2222", "p0102")');
        transaction.executeSql('INSERT INTO  disciplinaProfessor(codDisciplina, cadastro) VALUES ("d5555", "p0102")');
        transaction.executeSql('INSERT INTO  disciplinaProfessor(codDisciplina, cadastro) VALUES ("d3333", "p0103")');
        transaction.executeSql('INSERT INTO  disciplinaProfessor(codDisciplina, cadastro) VALUES ("d4444", "p0104")');
        transaction.executeSql('INSERT INTO  disciplinaProfessor(codDisciplina, cadastro) VALUES ("d5555", "p0101")');
        transaction.executeSql('INSERT INTO  disciplinaProfessor(codDisciplina, cadastro) VALUES ("d4444", "p0103")');
    });

    db.transaction(function(transaction) {
        //ALUNO
        transaction.executeSql('CREATE TABLE aluno (matricula,nome, codturma, senha)');
        transaction.executeSql('INSERT INTO aluno (matricula,nome, codturma, senha) VALUES (1111,"MATHEUS", 123,"t1111", 1111)');
        transaction.executeSql('INSERT INTO aluno (matricula,nome,codturma, senha) VALUES (2222,"FLAUBER", 234,"t2222", 2222)');
        transaction.executeSql('INSERT INTO aluno (matricula,nome, codturma, senha) VALUES (3333,"ERIVAN", 345, "t3333", 3333)');
        transaction.executeSql('INSERT INTO aluno (matricula,nome, codturma, senha) VALUES (4444,"ÉRICLES", 456,"t4444", 4444)');
        transaction.executeSql('INSERT INTO aluno (matricula,nome, codturma, senha) VALUES (5555,"RAFAEL", 567, "t5555", 5555)');
        transaction.executeSql('INSERT INTO aluno (matricula,nome, codturma, senha) VALUES (6666,"IRENE", 678,  "t6666", 6666)');
        transaction.executeSql('INSERT INTO aluno (matricula,nome,  codturma, senha) VALUES (7777,"JANAÍNA", 789,"t7777", 7777)');
    });

    db.transaction(function(transaction) {
        //TURMA
        transaction.executeSql('CREATE TABLE turma (codTurma,codcurso, serie, turno)');
        transaction.executeSql('INSERT INTO turma (codTurma,codcurso, serie, turno) VALUES (1111,"c1111","3º", "vespertino")');
        transaction.executeSql('INSERT INTO turma (codTurma,codcurso, serie, turno) VALUES (2222,"c4444","2º", "vespertino")');
        transaction.executeSql('INSERT INTO turma (codTurma,codcurso, serie, turno) VALUES (3333,"c3333","1º", "Matutino")');
        transaction.executeSql('INSERT INTO turma (codTurma,codcurso, serie, turno) VALUES (4444,"c2222","4º", "vespertino")');
    });

    db.transaction(function(transaction) {
        transaction.executeSql('SELECT * FROM aluno', [], function(transaction, results) {
            len = results.rows.length, i;

            for (i = 0; i < results.rows.length; i++) {
                Anome[i] = results.rows.item(i).nome;
                Amatricula[i] = results.rows.item(i).matricula;
                Acpf[i] = results.rows.item(i).cpf;
                Acodturma[i] = results.rows.item(i).codturma;
                Asenha[i] = results.rows.item(i).senha;

		alunosJSON.nome.push(results.rows.item(i).nome);
                alunosJSON.matricula.push( results.rows.item(i).matricula);

                $('<label><input type="checkbox" id="' + Amatricula[i] + '" name="aluno_ausente" data-iconpos="right" data-theme="a"  value="' + Anome[i] + '">' + Anome[i] + '</label>').appendTo("#chamada_lista").trigger("create");

            }
            
        }, null);
    });

    db.transaction(function(transaction) {
        transaction.executeSql('SELECT * FROM turma', [], function(transaction, results) {
            for (i = 0; i < results.rows.length; i++) {
                Tcodcurso[i] = results.rows.item(i).codcurso;
                Tcodturma[i] = results.rows.item(i).codTurma;
                Tserie[i] = results.rows.item(i).serie;
                Tturno[i] = results.rows.item(i).turno;
                $('<li data-theme="a" > <a href="#escolherData" onclick="getTurmaId2(' + Tcodturma[i] + ')" class="turmas" id="' + Tcodturma[i] + '" data-transition="slidedown"> ' + Tcodcurso[i] + ' ' + Tcodturma[i] + ' ' + Tserie[i] + 'serie </a></li>').appendTo("#visualizarList").trigger("create");
            }
        });
    });
    db.transaction(function(transaction) {

        transaction.executeSql('SELECT * FROM turma', [], function(transaction, results) {
            for (i = 0; i < results.rows.length; i++) {
                Tcodcurso[i] = results.rows.item(i).codcurso;
                Tcodturma[i] = results.rows.item(i).codTurma;
                Tserie[i] = results.rows.item(i).serie;
                Tturno[i] = results.rows.item(i).turno;

                $('<li data-theme="a" > <a href="#chamada" onclick="getTurmaId(' + Tcodturma[i].toString() + ')" class="turmas" id="' + Tcodturma[i] + '" data-transition="slidedown"> ' + Tcodcurso[i] + ' ' + Tcodturma[i] + ' ' + Tserie[i] + 'serie </a></li>').appendTo("#diarios_list").trigger("create");
            }

        }, null);

    });
}

function connectDB() {
    getProfessorInfo();
    getCursoInfo();
    getModalidadeInfo();
    getGradeInfo();
    getDisciplinaInfo();
    getDiscProfInfo();
    getDiscGradeInfo();
    getProfTurmaInfo();
    getBoletimInfo();
    //getFrequenciaInfo();
    //getAulaInfo();
}



function getDataSelecionada(data) {
    dataSelecionada = data;
    $("#visualizarChamada").html("");
    db = openDatabase("bdTeste9app", "1.0", "App Docente", 1024);
    var nome_alunos = new Array();

    db.transaction(function(transaction) {
        transaction.executeSql('SELECT * FROM frequencia WHERE aula_data=\'' + dataSelecionada + '\' AND codTurma=' + turmaEscolhida, [], function(transaction, results) {
            len = results.rows.length, i;
            var presenca;
            for (i = 0; i < results.rows.length; i++) {
                Faula_data[i] = results.rows.item(i).aula_data;
                Fmatricula[i] = results.rows.item(i).matricula;
                Fpresenca[i] = results.rows.item(i).pres;
                FcodTurma[i] = results.rows.item(i).codTurma;
                if (Fpresenca[i] === '-') {
                    Fpresenca[i] = 'ausente';
                } else {
                    Fpresenca[i] = 'presente';
                }
            }
            var index = 0;
            for (z = 0; z < Fmatricula.length; z++) {
                transaction.executeSql('Select * FROM aluno where matricula =   ' + Fmatricula[z], [], function(transaction, results) {
                    if (index < Fmatricula.length) {
                        $('<tr><td> <h2>' + results.rows.item(0).nome + ':</h2></td><td><h3>' + Fpresenca[index] + ' </td></h3></tr>').appendTo("#visualizarChamada").trigger("create");
                        index++;
                    }
                });
            }
        });

    });
    db.transaction(function(transaction) {
        transaction.executeSql('SELECT * FROM aula WHERE data=\'' + dataSelecionada + '\' AND codTurma=' + turmaEscolhida, [], function(transaction, results) {
            len = results.rows.length, i;
            for (i = 0; i < results.rows.length; i++) {
                AUassunto = results.rows.item(i).assunto;
                AUcadastro = results.rows.item(i).cadastro;
                AUcodTurma = results.rows.item(i).codTurma;
                AUdata = results.rows.item(i).data;
                AUqtdAulas = results.rows.item(i).qtdAulas;
            }
            $('<br> <br> <h2> Assunto:' + AUassunto + '</h2>').appendTo("#visualizarChamada").trigger("create");
            $('<h2>Quantidade de Aulas: ' + AUqtdAulas + '</h2>').appendTo("#visualizarChamada").trigger("create");
            $('<h2>Data: ' + AUdata + '</h2>').appendTo("#visualizarChamada").trigger("create");

            $('<br>').appendTo("#visualizarChamada").trigger("create");
            $('<br>').appendTo("#visualizarChamada").trigger("create");
           
            /*var serie, curso;
            var codcurso;
            transaction.executeSql('Select * FROM turma where codTurma =   ' + AUcodTurma, [], function(transaction, resultsa) {
                serie = resultsa.rows.item(0).serie;
                codcurso = resultsa.rows.item(0).codcurso;
            }); 
            transaction.executeSql('Select * FROM curso where codCurso=\'' + codcurso, [], function(transaction, results) {
                alert( results.rows.item(0).curso);
                $('<h1>Turma: ' +  results.rows.item(0).curso + " " + serie + '</h1>').appendTo("#visualizarChamada").trigger("create");
            }); */           
        });
    });

}

function sincronizar() {
    if (navigator.onLine) {
        alert("Sincronizando");
        $('#home_nome').html(atualUser);
        $('#home_matricula').html(atualUserCod);
        var serializedJSON = JSON.stringify(alunosJSON);
	var json = JSON.parse(serializedJSON);
        var xmlHttp = null;
        xmlHttp = new XMLHttpRequest();
        xmlHttp.open( "GET", "3as.info/educacao/", true );
        xmlHttp.send( null );
        alert (xmlHttp.statusText);
        
    } else {
        alert("Você não possui conexão a internet. Tente mais tarde.");
    }

}


//GETTERS
function getTurmaId(id) {
    turmaEscolhida = id;
    //alert(id);
}
function getTurmaId2(id) {
    turmaEscolhida = id;
    $("#selecionarData").html("");
    db = openDatabase("bdTeste9app", "1.0", "App Docente", 1024);
    db.transaction(function(transaction) {
        transaction.executeSql('SELECT * FROM aula WHERE codTurma=' + turmaEscolhida + ' group by data', [], function(transaction, results) {
            for (i = 0; i < results.rows.length; i++) {
                data[i] = results.rows.item(i).data;
                $('<li data-theme="a"> <a href="#visualizardiario" onclick="getDataSelecionada(\'' + data[i] + '\')" data-transition="slidedown">' + data[i] + '</a></li>').appendTo("#selecionarData");
            }
            $('#selecionarData').listview('refresh');
        });
    });
}

function getBoletimInfo() {
    db = openDatabase("bdTeste9app", "1.0", "App Docente", 1024);
    db.transaction(function(transaction) {
        transaction.executeSql('SELECT * FROM boletim', [], function(transaction, results) {
            len = results.rows.length, i;
            for (i = 0; i < results.rows.length; i++) {
                Bmatricula[i] = results.rows.item(i).matricula;
                BcodDisciplina[i] = results.rows.item(i).codDisciplina;
                Bmedia[i] = results.rows.item(i).media;
                Brec[i] = results.rows.item(i).rec;
                Bfaltas[i] = results.rows.item(i).faltas;
                BcodModalidade[i] = results.rows.item(i).codModalidade;
                Bn1[i] = results.rows.item(i).n1;
                Bn2[i] = results.rows.item(i).n2;
                Bn3[i] = results.rows.item(i).n3;
                Bn4[i] = results.rows.item(i).n4;
            }
        });
    });
}

function getDiscGradeInfo() {
    db = openDatabase("bdTeste9app", "1.0", "App Docente", 1024);
    db.transaction(function(transaction) {
        transaction.executeSql('SELECT * FROM disciplinaGrade', [], function(transaction, results) {
            len = results.rows.length, i;
            for (i = 0; i < results.rows.length; i++) {
                DGcodDisc[i] = results.rows.item(i).codDisciplina;
                DGcodGrade[i] = results.rows.item(i).codGrade;
            }
        });
    });
}

function getProfTurmaInfo() {
    db = openDatabase("bdTeste9app", "1.0", "App Docente", 1024);
    db.transaction(function(transaction) {
        transaction.executeSql('SELECT * FROM professorTurma', [], function(transaction, results) {
            len = results.rows.length, i;
            for (i = 0; i < results.rows.length; i++) {
                PTcadProf[i] = results.rows.item(i).cadastro;
                PTcodTurma[i] = results.rows.item(i).codTurma;
            }
        });
    });
}

function getDisciplinaInfo() {
    db = openDatabase("bdTeste9app", "1.0", "App Docente", 1024);
    db.transaction(function(transaction) {
        transaction.executeSql('SELECT * FROM disciplina', [], function(transaction, results) {
            len = results.rows.length, i;
            for (i = 0; i < results.rows.length; i++) {
                Dcod[i] = results.rows.item(i).codDisciplina;
                Ddisc[i] = results.rows.item(i).disciplina;
                Dch[i] = results.rows.item(i).ch;
            }
        });
    });
}

function getCursoInfo() {
    db = openDatabase("bdTeste9app", "1.0", "App Docente", 1024);
    db.transaction(function(transaction) {
        transaction.executeSql('SELECT * FROM curso', [], function(transaction, results) {
            len = results.rows.length, i;
            for (i = 0; i < results.rows.length; i++) {
                Ccod[i] = results.rows.item(i).codCurso;
                Ccurso[i] = results.rows.item(i).curso;
                Cgrade[i] = results.rows.item(i).codGrade;
            }
        });
    });
}

function getProfessorInfo() {
    db = openDatabase("bdTeste9app", "1.0", "App Docente", 1024);
    db.transaction(function(transaction) {
        transaction.executeSql('SELECT * FROM professor', [], function(transaction, results) {
            len = results.rows.length, i;
            for (i = 0; i < results.rows.length; i++) {
                Pnome[i] = results.rows.item(i).nome;
                Pcad[i] = results.rows.item(i).cadastro;
                Psenha[i] = results.rows.item(i).senha.toString();
            }

        }, null);
    });
}

function getAlunoInfo() {
    db = openDatabase("bdTeste9app", "1.0", "App Docente", 1024);

    db.transaction(function(transaction) {
        transaction.executeSql('SELECT * FROM aluno', [], function(transaction, results) {
            len = results.rows.length, i;

            for (i = 0; i < results.rows.length; i++) {
                Anome[i] = results.rows.item(i).nome;
                Amatricula[i] = results.rows.item(i).matricula;
                Acpf[i] = results.rows.item(i).cpf.toString();
                Acodturma[i] = results.rows.item(i).codturma;
                Asenha[i] = results.rows.item(i).senha.toString();

                $('<label><input type="checkbox" id="' + Amatricula[i] + '" name="aluno_ausente" data-iconpos="right" data-theme="a"  value="' + Anome[i] + '">' + Anome[i] + '</label>').appendTo("#chamada_lista").trigger("create");

            }

        }, null);
    });
}

function getGradeInfo() {
    db = openDatabase("bdTeste9app", "1.0", "App Docente", 1024);
    db.transaction(function(transaction) {
        transaction.executeSql('SELECT * FROM grade', [], function(transaction, results) {
            len = results.rows.length, i;
            for (i = 0; i < results.rows.length; i++) {
                Gcod[i] = results.rows.item(i).codGrade;
            }
        });
    });
}

function getModalidadeInfo() {
    db = openDatabase("bdTeste9app", "1.0", "App Docente", 1024);
    db.transaction(function(transaction) {
        transaction.executeSql('SELECT * FROM modalidade', [], function(transaction, results) {
            len = results.rows.length, i;
            for (i = 0; i < results.rows.length; i++) {
                Mcod[i] = results.rows.item(i).codModalidade;
                Mmodalidade[i] = results.rows.item(i).modalidade;
                Mperiodo[i] = results.rows.item(i).periodo;
            }
        });
    });
}


function getDiscProfInfo() {
    db = openDatabase("bdTeste9app", "1.0", "App Docente", 1024);
    db.transaction(function(transaction) {
        transaction.executeSql('SELECT * FROM disciplinaProfessor', [], function(transaction, results) {
            len = results.rows.length, i;
            for (i = 0; i < results.rows.length; i++) {
                DPcodDisc[i] = results.rows.item(i).codDisciplina;
                DPcadProf[i] = results.rows.item(i).cadastro;
            }
        });
    });
}




//INSERTS
function insertIntoFrequencia(imatricula, iaula_data, ipres) {
    db = openDatabase("bdTeste9app", "1.0", "App Docente", 1024);

    db.transaction(function(transaction) {
        transaction.executeSql('INSERT INTO frequencia (matricula, aula_data, pres) VALUES (?, ? ,?)', [imatricula, iaula_data, ipres]);
    });

}

function insertIntoAula(idata, iqtdaulas, icadastro, icodturma, iassunto) {
    db = openDatabase("bdTeste9app", "1.0", "App Docente", 1024);

    db.transaction(function(transaction) {
        transaction.executeSql('INSERT INTO frequencia (data, qtdAulas, cadastro, codTurma, assunto) VALUES (?, ?, ?, ?, ?)', [idata, iqtdaulas, icadastro, icodturma, iassunto]);
    });

}

function insertIntoBoletim(imatricula, icodDisciplina, irec, ifaltas, icodModalidade, in1, in2, in3, in4) {
    db = openDatabase("bdTeste9app", "1.0", "App Docente", 1024);

    db.transaction(function(transaction) {
        transaction.executeSql('INSERT INTO boletim (matricula, codDisciplina, media, rec, faltas, codModalidade, n1, n2, n3, n4) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)', [imatricula, icodDisciplina, (in1 * 2 + in2 * 2 + in3 * 3 + 3 * in4) / 10, irec, ifaltas, icodModalidade, in1, in2, in3, in4]);
    });

}

function insertIntoAluno(imatricula, inome, icpf, icodturma, isenha) {
    db = openDatabase("bdTeste9app", "1.0", "App Docente", 1024);

    db.transaction(function(transaction) {
        transaction.executeSql('INSERT INTO aluno (matricula,nome, cpf, codturma, senha) VALUES (?, ?, ?, ?, ?)', [imatricula, inome, icpf, icodturma, isenha]);
    });
}

function insertIntoTurma(icodTurma, icodcurso, iserie, iturno) {
    db = openDatabase("bdTeste9app", "1.0", "App Docente", 1024);

    db.transaction(function(transaction) {
        transaction.executeSql('INSERT INTO turma (codTurma,codcurso, serie, turno)  VALUES (?, ?, ?, ?)', [icodTurma, icodcurso, iserie, iturno]);
    });
}

function insertIntoProfessor(icadastro, inome, isenha) {
    db = openDatabase("bdTeste9app", "1.0", "App Docente", 1024);

    db.transaction(function(transaction) {
        transaction.executeSql('INSERT INTO  professor(cadastro, nome, senha)  VALUES (?, ?, ?)', [icadastro, inome, isenha]);
    });
}

function insertIntoCurso(icodCurso, icurso, icodGrade) {
    db = openDatabase("bdTeste9app", "1.0", "App Docente", 1024);

    db.transaction(function(transaction) {
        transaction.executeSql('INSERT INTO  curso(codCurso, curso, codGrade)  VALUES (?, ?, ?)', [icodCurso, icurso, icodGrade]);
    });
}

function insertIntoGrade(icodGrade) {
    db = openDatabase("bdTeste9app", "1.0", "App Docente", 1024);

    db.transaction(function(transaction) {
        transaction.executeSql('INSERT INTO grade(codGrade) VALUES (?)', [icodGrade]);
    });
}

function insertIntoModalidade(icodModalidade, imodalidade, iperiodo) {
    db = openDatabase("bdTeste9app", "1.0", "App Docente", 1024);

    db.transaction(function(transaction) {
        transaction.executeSql('INSERT INTO   modalidade(codModalidade, modalidade, periodo)  VALUES (?, ?, ?)', [icodModalidade, imodalidade, iperiodo]);
    });
}

function insertIntoDiscGrade(icodGrade, icodDisciplina) {
    db = openDatabase("bdTeste9app", "1.0", "App Docente", 1024);

    db.transaction(function(transaction) {
        transaction.executeSql('INSERT INTO   disciplinaGrade(codGrade, codDisciplina)  VALUES (?, ?)', [icodGrade, icodDisciplina]);
    });
}

function insertIntoProfTurma(icadastro, icodTurma) {
    db = openDatabase("bdTeste9app", "1.0", "App Docente", 1024);

    db.transaction(function(transaction) {
        transaction.executeSql('INSERT INTO    professorTurma(cadastro, codTurma)  VALUES (?, ?)', [icadastro, icodTurma]);
    });
}

function insertIntoProfDisciplina(icodDisciplina, icadastro) {
    db = openDatabase("bdTeste9app", "1.0", "App Docente", 1024);

    db.transaction(function(transaction) {
        transaction.executeSql('INSERT INTO   disciplinaProfessor(codDisciplina, cadastro)  VALUES (?, ?)', [icodDisciplina, icadastro]);
    });
}

//UPDATES